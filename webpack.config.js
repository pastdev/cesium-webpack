const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require("path");
const webpack = require("webpack");

const root = path.resolve(__dirname);
const dist = path.join(root, "dist");

const cesiumSource = path.join(root, 'node_modules', 'cesium', 'Build', 'Cesium');
const cesiumDist = path.join(dist, 'cesium');

module.exports = {
    entry: {
        app: path.join(root, "src", "app.js"),
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    {loader: 'style-loader'},
                    {loader: 'css-loader'},
                ],
            },
            { 
                test: /\.(png|gif|jpg|jpeg)$/,
                use: [{loader: "file-loader"}],
            },
        ],
    },
    output: {
        path: dist,
        filename: "[name].js",
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: "index.html",
            template: path.join(root, "src", "index.ejs"),
        }),
        new CopyWebpackPlugin([
            {from: cesiumSource, to: cesiumDist},
        ]),
    ],
};
